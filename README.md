# HTML cleaner

Web app for html cleaning, built with _Flask_ and _BeautifulSoup_.

Hosted at: https://hclean.mdr.lv/

### Screnshots from latest release

![Hcleaner uses TinyMCE](screenshot1.png)

![Choose what to remove](screenshot2.png)


### Older screnshot

![bootstrap4](screenshot0.jpg)
