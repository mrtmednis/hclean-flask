from bs4 import BeautifulSoup
from flask import (
    Flask,
    render_template,
    request,
)

from forms import Form1

# Initialize the Flask application
application = Flask(__name__)
application.config["SECRET_KEY"] = "hohohoo"


@application.route("/", methods=["GET", "POST"])
def consolidation_page():
    form = Form1()

    if request.method == "POST" and form.validate():
        ATTR = [
            "style",
            "class",
            "width",
            "height",
            "lang",
            "align",
            "face",
            "size",
            "cellspacing",
            "cellpadding",
            "dir",
            "role",
            "aria-level",
        ]

        TAGS = ["font", "span", "u", "div", "colgroup", "col"]
        SIMPL = ["td_p", "td_p_strong", "strong_strong", "empty_anchors", "empty_p"]

        attr = ["c_{}".format(x) in request.form for x in ATTR]
        tags = ["c_{}".format(x) in request.form for x in TAGS]
        simpl = ["c_{}".format(x) in request.form for x in SIMPL]

        htmlcode = clean_html1(
            html=request.form["text"],
            attr=[x[1] for x in zip(attr, ATTR) if x[0]],
            tags=[x[1] for x in zip(tags, TAGS) if x[0]],
            simpl=[x[1] for x in zip(simpl, SIMPL) if x[0]],
        )

        form.text.data = htmlcode

        return render_template(
            "myform.html", form=form
        )
    else:
        return render_template(
            "myform.html", form=form
        )


# def attr_filter(name):
#     return [r' {}=".*?"'.format(name), r" {}=\d+".format(name)]


# def tag_filter(name):
#     return [r"<{}.*?>".format(name), r"</{}>".format(name)]


# def clean_html(data, **kwargs):
#     if "attr" in kwargs:
#         for f in kwargs["attr"]:
#             for r in attr_filter(f):
#                 data = re.sub(r, "", data)
#     if "tags" in kwargs:
#         for f in kwargs["tags"]:
#             for r in tag_filter(f):
#                 data = re.sub(r, "", data)
#     return data


def clean_html1(
    *, html: str, tags: list[str], attr: list[str], simpl: list[str]
) -> str:
    # print(f"attr= {attr}")
    # print(f"tags= {tags}")
    # print(f"simpl= {simpl}")

    soup = BeautifulSoup(html, "html.parser")

    # 1. Remove attributes
    for tag in soup.find_all(True):  # True finds all tags
        tag.attrs = {key: value for key, value in tag.attrs.items() if key not in attr}

    # 2. Remove empty anchor tags
    if "empty_anchors" in simpl:
        for a in soup.find_all("a"):
            if not a.get_text(strip=True):  # No visible text
                a.decompose()

    # 3. Remove <p> tags from tables if there's only one paragraph in a cell
    if "td_p" in simpl:
        for td in soup.find_all("td"):
            ps = td.find_all("p")
            if len(ps) == 1:
                ps[0].unwrap()  # Removes <p> but keeps the text

    # 4. Replace <i> with <em> and <b> with <strong>
    for i_tag in soup.find_all("i"):
        i_tag.name = "em"
    for b_tag in soup.find_all("b"):
        b_tag.name = "strong"

    # consolidate strong tags in cells
    if "strong_strong" in simpl:
        for td in soup.find_all("td"):
            strong_tags = td.find_all("strong")

            # Check if all content in <td> is consecutive <strong> tags
            if len(strong_tags) > 1 and all(
                tag.name == "strong" for tag in td.contents if tag.name == "strong"
            ):
                # Combine text from all <strong> tags
                combined_text = "".join(tag.get_text() for tag in strong_tags)

                # Clear the td and replace with a single <strong>
                td.clear()
                new_strong = soup.new_tag("strong")
                new_strong.string = combined_text
                td.append(new_strong)

        # consolidate strong tags in paragraphs
        for td in soup.find_all("p"):
            strong_tags = td.find_all("strong")

            # Check if all content in <td> is consecutive <strong> tags
            if len(strong_tags) > 1 and all(
                tag.name == "strong" for tag in td.contents if tag.name == "strong"
            ):
                # Combine text from all <strong> tags
                combined_text = "".join(tag.get_text() for tag in strong_tags)

                # Clear the td and replace with a single <strong>
                td.clear()
                new_strong = soup.new_tag("strong")
                new_strong.string = combined_text
                td.append(new_strong)

    # 5. Convert <td> to <th> if it contains only <p><strong>...</strong></p> or <strong>...</strong>
    if "td_p_strong" in simpl:
        for td in soup.find_all("td"):
            # #Case 1: <p><strong>...</strong></p>
            # p = td.find('p')
            # if p and len(p.contents) == 1 and p.contents[0].name == 'strong':
            #     td.string = p.contents[0].get_text(strip=True)
            #     td.name = 'th'
            #     continue  # Skip to next <td>

            # Case 2: <strong>...</strong> without <p>
            strong = td.find("strong")
            if len(td.contents) == 1 and strong and strong == td.contents[0]:
                td.string = strong.get_text(strip=True)
                td.name = "th"

    # 6. Remove requested tags but keep their contents
    for tag_to_remove in tags:
        for tag in soup.find_all(tag_to_remove):
            tag.unwrap()  # Removes <span> but keeps the inner content

    # # 7. Remove <u> tags but keep their contents
    # if "u" in tags:
    #     for span in soup.find_all('u'):
    #         span.unwrap()  # Removes <span> but keeps the inner content

    # # 8. Remove <div> tags but keep their contents
    # if "div" in tags:
    #     for span in soup.find_all('div'):
    #         span.unwrap()  # Removes <span> but keeps the inner content

    # 7. Remove empty <p> tags and <p> tags with only spaces
    if "empty_p" in simpl:
        for p in soup.find_all("p"):
            if not p.get_text(strip=True):  # Empty or contains only whitespace
                p.decompose()

    return str(soup)
